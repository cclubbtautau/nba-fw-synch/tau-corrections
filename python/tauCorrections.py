import os
import envyaml

from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/TAU/python/tauCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class tauSFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(tauSFRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year");  year = str(self.year)
        self.vsjet_wps = kwargs.pop("vsjet_wps", ["Medium", "Tight"])
        self.vse_wps = kwargs.pop("vse_wps", ["VVLoose", "Tight"])
        self.vsmu_wps = kwargs.pop("vsmu_wps", ["VLoose", "Tight"])
        self.vsjet_vse_wp = kwargs.pop("vsjet_vse_wp", "VVLoose")
        self.vsjet_uncertainties = kwargs.pop("vsjet_uncertainties", ["nom", "up", "down"])
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix+year

        try:
            self.tauIdAlgo = kwargs.pop("tauId_algo")
            self.tauIdAlgo = self.tauIdAlgo[2:]
        except KeyError:
            self.tauIdAlgo = corrCfg[self.corrKey]["corrName"]
            print("** WARNING: tau ID algorithm specified for corrections! "
                  "Defaulting to correction name from Yaml: "+self.tauIdAlgo)

        # read automatically defined uncertainty splitting from the correction YAML
        if self.vsjet_uncertainties == "automatic":
            if self.year <= 2018:
                raise ValueError("Automatic systematics splitting implemented only for Run-3 datasets!")

            self.vsjet_uncertainties = ["nom"]

            for syst in corrCfg["id_systematics"][self.corrKey]:
                # skip all the cases for which the systematic variation would not actually be used
                if self.skip_unused_systs and self.systs != "":
                    continue
                self.vsjet_uncertainties.append(syst)

            for syst in corrCfg["tes_systematics"][self.corrKey]:
                if not syst: continue # safety against None
                tau_syst_drctn = self.tau_syst.split("_")[-1]
                syst_drctn = syst[0].split("_")[-1]
                # skip all the cases for which the systematic variation would not actually be used
                if self.skip_unused_systs and (self.tau_syst == self.tau_central
                                                or tau_syst_drctn != syst_drctn):
                    continue
                self.vsjet_uncertainties.append(syst[0])

        else:
            tmp_unc = self.vsjet_uncertainties
            self.vsjet_uncertainties = []
            for syst in tmp_unc:
                # store systematics impact on SF only for the central production;
                # skip them for all systematic variated selections
                if self.skip_unused_systs and self.systs != "" and syst != "nom":
                    continue
                self.vsjet_uncertainties.append(syst)

        if self.isMC:
            if not isUL and self.year <= 2018:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_TauSF"):
                os.environ["_TauSF"] = "_TauSF"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")
                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vsjet = MyCorrections("%s", "%s");' %
                        (corrCfg[self.corrKey]["fileName"], self.tauIdAlgo+"VSjet"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vse = MyCorrections("%s", "%s");' %
                        (corrCfg[self.corrKey]["fileName"], self.tauIdAlgo+"VSe"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vsmu = MyCorrections("%s", "%s");' %
                        (corrCfg[self.corrKey]["fileName"], self.tauIdAlgo+"VSmu"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tes = MyCorrections("%s", "%s");' %
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrNameTES"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_deeptau_vsjet_sf(
                            Vfloat pt, Vint dm, Vint genmatch,
                            std::string wp, std::string wp_vse, std::string syst, std::string flag) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (dm[i] > 2 && dm[i] < 10) {
                                sf.push_back(1.); // SFs only provided for DM 0,1,10,11
                            }
                            else 
                                sf.push_back(corr_vsjet.eval({pt[i], dm[i], genmatch[i], wp, wp_vse, syst, flag}));
                        }
                        return sf;
                    }

                    ROOT::RVec<double> get_deeptau_vse_sf(
                            Vfloat eta, Vint dm, Vint genmatch, std::string wp, 
                            std::string syst, std::string idV) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < eta.size(); i++) {
                            if (dm[i] >= 2 && dm[i] < 10) sf.push_back(1.);
                            else if (idV == "DeepTau2017v2p1") {
                                sf.push_back(corr_vse.eval({eta[i], genmatch[i], wp, syst}));
                            }
                            else { 
                                sf.push_back(corr_vse.eval({eta[i], dm[i], genmatch[i], wp, syst}));
                            }
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_deeptau_vsmu_sf(
                            Vfloat eta, Vint genmatch, std::string wp, std::string syst) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < eta.size(); i++) {
                            sf.push_back(corr_vsmu.eval({eta[i], genmatch[i], wp, syst}));
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_tes(
                            Vfloat pt, Vfloat eta, Vint dm, Vint genmatch, std::string idV,
                            std::string wp, std::string wp_vse, std::string syst) {
                        ROOT::RVec<double> tes_factor;
                        for (size_t i = 0; i < eta.size(); i++) {
                            if (dm[i] == 5 || dm[i] == 6) tes_factor.push_back(1.);
                            else if (idV == "DeepTau2017v2p1") {
                                tes_factor.push_back(corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, syst}));
                            }
                            else {
                                tes_factor.push_back(corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, wp, wp_vse, syst}));
                            }
                        }
                        return tes_factor;
                    }
                    std::vector<ROOT::RVec<double>> get_tes_perDM(
                            Vfloat pt, Vfloat eta, Vint dm, Vint genmatch, std::string idV,
                            std::string wp, std::string wp_vse, std::string syst) {
                        ROOT::RVec<double> tes_factor;
                        ROOT::RVec<double> tes_factorDM0;
                        ROOT::RVec<double> tes_factorDM1;
                        ROOT::RVec<double> tes_factorDM10;
                        ROOT::RVec<double> tes_factorDM11;
                        double sf = 1.;
                        for (size_t i = 0; i < eta.size(); i++) {
                            if (dm[i] == 5 || dm[i] == 6) {
                                tes_factorDM0.push_back(1.);  tes_factorDM1.push_back(1.);
                                tes_factorDM10.push_back(1.); tes_factorDM11.push_back(1.);
                                tes_factor.push_back(1.);
                            }
                            else if (idV == "DeepTau2017v2p1") {
                                sf = corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, syst});
                                tes_factor.push_back(sf);

                                if (dm[i] == 0) tes_factorDM0.push_back(sf);
                                else            tes_factorDM0.push_back(1.);

                                if (dm[i] == 1) tes_factorDM1.push_back(sf);
                                else            tes_factorDM1.push_back(1.);

                                if (dm[i] == 10) tes_factorDM10.push_back(sf);
                                else             tes_factorDM10.push_back(1.);

                                if (dm[i] == 11) tes_factorDM11.push_back(sf);
                                else             tes_factorDM11.push_back(1.);

                            }
                            else {
                                sf = corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, wp, wp_vse, syst});
                                tes_factor.push_back(sf);

                                if (dm[i] == 0) tes_factorDM0.push_back(sf);
                                else            tes_factorDM0.push_back(1.);

                                if (dm[i] == 1) tes_factorDM1.push_back(sf);
                                else            tes_factorDM1.push_back(1.);

                                if (dm[i] == 10) tes_factorDM10.push_back(sf);
                                else             tes_factorDM10.push_back(1.);

                                if (dm[i] == 11) tes_factorDM11.push_back(sf);
                                else             tes_factorDM11.push_back(1.);

                            }
                        }
                        return {tes_factor, tes_factorDM0, tes_factorDM1, tes_factorDM10, tes_factorDM11};
                    }
                """)

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []

        # Tau Energy Scale
        for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
            # skip all the cases for which the systematic variation would not actually be used
            if self.skip_unused_systs:
                if self.tau_syst == self.tau_central and syst != "nom":
                    continue
                if self.tau_syst != self.tau_central and syst != "nom" and syst_name not in self.tau_syst:
                    continue

            # tes factor is WP dependent for DeepTau2018v2p5
            if self.year >= 2022:
                for wp in self.vsjet_wps:
                    df = df.Define("tes_factors_perDM%s" % syst_name,
                                    'get_tes_perDM(Tau_pt, Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "%s")' %
                                    (self.tauIdAlgo, wp, self.vsjet_vse_wp, syst))

                    df = df.Define("tes_factor%s" % syst_name,  "tes_factors_perDM%s[0]" % syst_name)
                    df = df.Define("Tau_pt%s%s" % (self.tau_central,syst_name),
                                    "Tau_pt * tes_factor%s" % syst_name)
                    df = df.Define("Tau_mass%s%s" % (self.tau_central,syst_name),
                                    "Tau_mass * tes_factor%s" % syst_name)

                    branches.append("tes_factor%s" % syst_name)
                    branches.append("Tau_pt%s%s" % (self.tau_central,syst_name))
                    branches.append("Tau_mass%s%s" % (self.tau_central,syst_name))

                    for dm, idx in zip(["DM0", "DM1", "DM10", "DM11"], [1, 2, 3, 4]):
                        # skip all the cases for which the split tes would not actually be used
                        if not dm in self.tau_syst: # and self.tau_syst != self.tau_central:
                            continue

                        df = df.Define("tes_factor_%s%s" % (dm, syst_name),  "tes_factors_perDM%s[%i]" % (syst_name, idx))
                        df = df.Define("Tau_pt%s%s%s" % (self.tau_central, dm, syst_name),
                                        "Tau_pt * tes_factor_%s%s" % (dm, syst_name))
                        df = df.Define("Tau_mass%s%s%s" % (self.tau_central,dm, syst_name),
                                        "Tau_mass * tes_factor_%s%s" % (dm, syst_name))

                        branches.append("tes_factor_%s%s" % (dm, syst_name))
                        branches.append("Tau_pt%s%s%s" % (self.tau_central, dm, syst_name))
                        branches.append("Tau_mass%s%s%s" % (self.tau_central, dm, syst_name))

            # tes factor is NOT WP dependent for DeepTau2017v2p1
            elif self.year <= 2018:
                df = df.Define("tes_factor%s" % syst_name,
                                'get_tes(Tau_pt, Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "dummy", "dummy", "%s")' %
                                (self.tauIdAlgo, syst))

                df = df.Define("Tau_pt%s%s" % (self.tau_central,syst_name),
                                "Tau_pt * tes_factor%s" % syst_name)

                df = df.Define("Tau_mass%s%s" % (self.tau_central,syst_name),
                                "Tau_mass * tes_factor%s"  % syst_name)

                branches.append("tes_factor%s" % syst_name)
                branches.append("Tau_pt%s%s" % (self.tau_central,syst_name))
                branches.append("Tau_mass%s%s" % (self.tau_central,syst_name))

        # DeepTau vsJet scale factor
        for syst in self.vsjet_uncertainties:
            syst_name = "" if syst == "nom" else "_"+syst

            for wp in self.vsjet_wps:
                # pt binned SFs are available only for DeepTau2017v2p1
                if self.tauIdAlgo == "DeepTau2017v2p1":
                    df = df.Define("Tau_sf%sVSjet_pt_binned_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name),
                                    f'get_deeptau_vsjet_sf(Tau_pt{self.tau_syst}, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "pt")' %
                                    (wp, self.vsjet_vse_wp, syst))

                    branches.append("Tau_sf%sVSjet_pt_binned_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name))

                df = df.Define("Tau_sf%sVSjet_dm_binned_%s%s" %
                                (self.tauIdAlgo, wp, syst_name),
                                f'get_deeptau_vsjet_sf(Tau_pt{self.tau_syst}, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "dm")' %
                                (wp, self.vsjet_vse_wp, syst))

                branches.append("Tau_sf%sVSjet_dm_binned_%s%s" %
                                (self.tauIdAlgo, wp, syst_name))

        # DeepTau vsMu and vsEle scale factors
        for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
            # store systematics impact on SF only for the central production;
            # no need to store all of them for all systematic variated selections
            if not self.skip_unused_systs or (self.systs != "" and syst == "nom") or self.systs == "":
                for wp in self.vse_wps:
                    df = df.Define("Tau_sf%sVSe_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name),
                                    'get_deeptau_vse_sf(Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s")' %
                                    (wp, syst, self.tauIdAlgo))

                    branches.append("Tau_sf%sVSe_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name))

                for wp in self.vsmu_wps:
                    df = df.Define("Tau_sf%sVSmu_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name),
                                    'get_deeptau_vsmu_sf(Tau_eta, Tau_genPartFlav, "%s", "%s")' %
                                    (wp, syst))

                    branches.append("Tau_sf%sVSmu_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name))

        return df, branches


def tauSFRDF(**kwargs):
    """
    Module to obtain DeepTau2017v2p1/DeepTau2018v2p5 SFs and TES with their up/down uncertainties.
    Computes both pt and dm-binned SFs (applied in dauIdIso.py, choosing which one based on tau pt)

    :param vsjet_wps: DeepTau2017v2p1VSjet/DeepTau2018v2p5VSjet WPs to consider. Default: `[Medium, Tight]`
    :type vsjet_wps: list of str

    :param vse_wps: DeepTau2017v2p1VSe/DeepTau2018v2p5VSe WPs to consider.  Default: `[VVLoose, Tight]`
    :type vse_wps: list of str

    :param vsmu_wps: DeepTau2017v2p1VSmu/DeepTau2018v2p5VSmu WPs to consider. Default: `[VLoose, Tight]`
    :type vsmu_wps: list of str

    :param vsjet_vse_wp: DeepTau2017v2p1VSeWP/DeepTau2018v2p5VSeWP used to compute the DeepTau2017v2p1VSjet/DeepTau2018v2p5VSjet scale factor.
        Default: `VVLoose`
    :type vsjet_vse_wp: str

    :param vsjet_uncertainties: Uncertainties to be computed for DeepTau2017v2p1VSjet scale factor. 
        Possible values for DeepTau2017v2p1VSjet:
          down, stat1_dm0_down, stat1_dm0_up, stat1_dm10_down, stat1_dm10_up, stat1_dm11_down,
          stat1_dm11_up, stat1_dm1_down, stat1_dm1_up, stat2_dm0_down, stat2_dm0_up, stat2_dm10_down,  
          stat2_dm10_up, stat2_dm11_down, stat2_dm11_up, stat2_dm1_down, stat2_dm1_up,                 
          stat_highpT_bin1_down, stat_highpT_bin1_up, stat_highpT_bin2_down, stat_highpT_bin2_up,      
          stat_ptbin1_down, stat_ptbin1_up, stat_ptbin2_down, stat_ptbin2_up, stat_ptbin3_down,        
          stat_ptbin3_up, stat_ptbin4_down, stat_ptbin4_up, stat_ptbin5_down, stat_ptbin5_up,          
          stat_ptbin6_down, stat_ptbin6_up, stat_ptbin7_down, stat_ptbin7_up, stat_ptbin8_down,        
          stat_ptbin8_up, stat_ptbin9_down, stat_ptbin9_up, syst_2018_dm0_down, syst_2018_dm0_up,      
          syst_2018_dm10_down, syst_2018_dm10_up, syst_2018_dm11_down, syst_2018_dm11_up,              
          syst_2018_dm1_down, syst_2018_dm1_up, syst_2018_down, syst_2018_up, syst_alleras_down,       
          syst_alleras_up, syst_highpT_down, syst_highpT_extrap_down, syst_highpT_extrap_up,           
          syst_highpT_up, up
        Possible values for DeepTau2018v2p5VSjet 2022:
          down, stat1_dm0_down, stat1_dm0_up, stat1_dm10_down, stat1_dm10_up, stat1_dm11_down,
          stat1_dm11_up, stat1_dm1_down, stat1_dm1_up, stat2_dm0_down, stat2_dm0_up, stat2_dm10_down,
          stat2_dm10_up, stat2_dm11_down, stat2_dm11_up, stat2_dm1_down, stat2_dm1_up,
          stat_highpT_bin1_down, stat_highpT_bin1_up, stat_highpT_bin2_down, stat_highpT_bin2_up,
          syst_2022_postEE_down, syst_2022_postEE_up, syst_TES_2022_postEE_dm0_down,
          syst_TES_2022_postEE_dm0_up, syst_TES_2022_postEE_dm10_down, syst_TES_2022_postEE_dm10_up,
          syst_TES_2022_postEE_dm11_down, syst_TES_2022_postEE_dm11_up, syst_TES_2022_postEE_dm1_down,
          syst_TES_2022_postEE_dm1_up, syst_alleras_down, syst_alleras_up, syst_highpT_down,
          syst_highpT_extrap_down, syst_highpT_extrap_up, syst_highpT_up, up
        Possible values for DeepTau2018v2p5VSjet 2023:
          down, nom, stat_highpT_bin1_down, stat_highpT_bin1_up, syst_highpT_down,
          syst_highpT_extrap_down, syst_highpT_extrap_up, syst_highpT_up, up
        Default: [nom, up, down]
    :type vsjet_uncertainties: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: tauSFRDF
            path: Corrections.TAU.tauCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isUL: self.dataset.has_tag('ul')
                runPeriod: self.dataset.runPeriod
                tauId_algo: self.config.tauId_algo
                vsjet_wps: [Medium, Tight]
                vse_wps: [VVLoose, Tight]
                vsmu_wps: [VLoose, Tight]

    """

    return lambda: tauSFRDFProducer(**kwargs)
